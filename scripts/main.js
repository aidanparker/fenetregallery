'use strict';

$(function(){

  var navLinks = '#nav ul li ';
  var menuBtn = $('#menuBtn');
  var nav = $('#nav');
  var topBtn = $('#topBtn');
  var amount = 0;
  var i = 0;
  var docTitle = document.title.substr(0, document.title.indexOf('|') - 1);
  var docID = $('html').attr('id');

  /** OFFCANVAS NAV **/

  menuBtn.click(function(){
    menuBtn.toggleClass('open');
    nav.toggleClass('openNav');
    $('#nav a').toggleClass('animate');
  });

  /** HIGHLIGHT CURRENT PAGE ON NAV **/

  var url = window.location.pathname,
        urlRegExp = new RegExp(url.replace(/\/$/, '') + '$'); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
        // now grab every link from the navigation
        $(navLinks + 'a').each(function(){
            // and test its normalized href against the url pathname regexp
            var newExp = this.href.replace(/\/$/, '');
            if(window.location.href.indexOf(newExp) > -1){
                $(this).addClass('active');
            }



            if(String(urlRegExp) === '/$/' || docTitle === 'Home') {
              $(this).removeClass('active');
              $('#home').addClass('active');
            }

        });


  /** ANIMATE RETURN TO TOP BUTTON **/

  topBtn.click(function(e){
    e.preventDefault();
    $('html,body').animate({'scrollTop': 0}, 500);
  });

  /** POPULATE EXHIBIT PAGE **/

  $('section.perspective').addClass('animate');
  $('section.exhibit').addClass('animate');

  if(docID === 'exhibits'){
    amount = 6;

    for(i = amount; i > 0; i--){
      $('figure#image' + i + '  span.image').css('background-image', 'url(../images/exhibits/exhibit-' + i + '.jpg)');
    }


    $.getJSON('../scripts/exhibits.json', function(exhibits){
      $.each(exhibits, function(){
        $.each(this, function(k){
          var j = amount - k;
          $('figure#image' + j + '  figcaption').append('<strong>' + this.name + '</strong>' + this.artists);
          $('figure#image' + j + '  a').attr('href', this.link);
        });
      });
    });

  }

  if(docID === 'edges-of-light'){
    console.log('got here');
    amount = 6;

    for(i = amount; i > 0; i--){
      $('figure#image' + i + '  span.image').css('background-image', 'url(../images/exhibits/edges-of-light/thumbnails/thumbnail-' + i + '.jpg)');
      $('figure#image' + i + '  a').attr('href', '../images/exhibits/edges-of-light/full/full-' + i + '.jpg');
    }

  }

  if(docID === 'home'){
    amount = 4;
    var imgs = [1, 2, 3, 4, 5, 6];

    for(i = amount; i > 0; i--){
      var rp = Math.round(Math.random() * (imgs.length - 1));
      $('figure#image' + i + '  span.image').css('background-image', 'url(images/exhibits/edges-of-light/thumbnails/thumbnail-' + imgs[rp] + '.jpg)');
      imgs.splice(rp, 1);
    }

  }

});